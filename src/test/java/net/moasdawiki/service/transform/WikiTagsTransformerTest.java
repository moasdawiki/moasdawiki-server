/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2025 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.transform;

import net.moasdawiki.base.Logger;
import net.moasdawiki.base.Messages;
import net.moasdawiki.base.Settings;
import net.moasdawiki.service.repository.AnyFile;
import net.moasdawiki.service.wiki.WikiFile;
import net.moasdawiki.service.wiki.WikiHelper;
import net.moasdawiki.service.wiki.WikiService;
import net.moasdawiki.service.wiki.structure.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.*;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class WikiTagsTransformerTest {

    private Settings settings;
    private Messages messages;
    private WikiService wikiService;
    private WikiTagsTransformer wikiTagsTransformer;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        Logger logger = new Logger(null);
        settings = mock(Settings.class);
        when(settings.getActualTime()).thenReturn(new Date());
        messages = mock(Messages.class);
        when(messages.getMessage(anyString())).thenReturn("");
        wikiService = mock(WikiService.class);
        when(wikiService.getWikiFile("/path")).thenReturn(new WikiFile("/path", "content",
                new WikiPage("/path", null, null, null), new AnyFile("/path.txt", new Date())));
        wikiTagsTransformer = new WikiTagsTransformer(logger, settings, messages, wikiService);
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void testTableOfContents() {
        PageElementList content = new PageElementList();
        content.add(new TableOfContents());
        content.add(new Heading(1, new TextOnly("Heading 1")));
        content.add(new Heading(2, new TextOnly("Heading 1.1")));
        content.add(new Heading(3, new TextOnly("Heading 1.1.1")));
        content.add(new Heading(1, new TextOnly("Heading 2")));
        content.add(new Heading(1, new TextOnly("Heading 2")));
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof PageElementList);
        PageElementList resultPel = (PageElementList) result.getChild();
        assertEquals(resultPel.size(), 6);
        assertTrue(resultPel.get(0) instanceof PageElementList);
        PageElementList headingPel = (PageElementList) resultPel.get(0);
        assertEquals(headingPel.size(), 5);
        // heading 1
        assertTrue(headingPel.get(0) instanceof Paragraph);
        Paragraph h1 = (Paragraph) headingPel.get(0);
        assertEquals(h1.getIndention(), 1);
        assertTrue(h1.getChild() instanceof LinkPage);
        LinkPage h1Link = (LinkPage) h1.getChild();
        assertNull(h1Link.getPagePath()); // same page
        assertEquals(h1Link.getAnchor(), "Heading1");
        assertEquals(WikiHelper.getStringContent(h1Link.getChild()), "1. Heading 1");
        // heading 2
        assertTrue(headingPel.get(1) instanceof Paragraph);
        Paragraph h2 = (Paragraph) headingPel.get(1);
        assertEquals(h2.getIndention(), 2);
        assertTrue(h2.getChild() instanceof LinkPage);
        LinkPage h2Link = (LinkPage) h2.getChild();
        assertNull(h2Link.getPagePath()); // same page
        assertEquals(h2Link.getAnchor(), "Heading1.1");
        assertEquals(WikiHelper.getStringContent(h2Link.getChild()), "1.1. Heading 1.1");
        // heading 3
        assertTrue(headingPel.get(2) instanceof Paragraph);
        Paragraph h3 = (Paragraph) headingPel.get(2);
        assertEquals(h3.getIndention(), 3);
        assertTrue(h3.getChild() instanceof LinkPage);
        LinkPage h3Link = (LinkPage) h3.getChild();
        assertNull(h3Link.getPagePath()); // same page
        assertEquals(h3Link.getAnchor(), "Heading1.1.1");
        assertEquals(WikiHelper.getStringContent(h3Link.getChild()), "1.1.1. Heading 1.1.1");
        // heading 4
        assertTrue(headingPel.get(3) instanceof Paragraph);
        Paragraph h4 = (Paragraph) headingPel.get(3);
        assertEquals(h4.getIndention(), 1);
        assertTrue(h4.getChild() instanceof LinkPage);
        LinkPage h4Link = (LinkPage) h4.getChild();
        assertNull(h4Link.getPagePath()); // same page
        assertEquals(h4Link.getAnchor(), "Heading2");
        assertEquals(WikiHelper.getStringContent(h4Link.getChild()), "2. Heading 2");
        // heading 5
        assertTrue(headingPel.get(4) instanceof Paragraph);
        Paragraph h5 = (Paragraph) headingPel.get(4);
        assertEquals(h5.getIndention(), 1);
        assertTrue(h5.getChild() instanceof LinkPage);
        LinkPage h5Link = (LinkPage) h5.getChild();
        assertNull(h5Link.getPagePath()); // same page
        assertEquals(h5Link.getAnchor(), "Heading2-1");
        assertEquals(WikiHelper.getStringContent(h5Link.getChild()), "3. Heading 2");
    }

    @Test
    public void testParent() {
        Parent content = new Parent("/path");
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertNull(result.getChild());
    }

    @Test
    public void testWikiVersion() {
        WikiVersion content = new WikiVersion();
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof TextOnly);
    }

    @Test
    public void testDateTime_Time() {
        DateTime content = new DateTime(DateTime.Format.SHOW_TIME);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof TextOnly);
        verify(messages, times(1)).getMessage(eq(WikiTagsTransformer.DATEFORMAT_TIME));
        verify(settings, times(1)).getActualTime();
    }

    @Test
    public void testDateTime_Date() {
        DateTime content = new DateTime(DateTime.Format.SHOW_DATE);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof TextOnly);
        verify(messages, times(1)).getMessage(eq(WikiTagsTransformer.DATEFORMAT_DATE));
        verify(settings, times(1)).getActualTime();
    }

    @Test
    public void testDateTime_DateTime() {
        DateTime content = new DateTime(DateTime.Format.SHOW_DATETIME);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof TextOnly);
        verify(messages, times(1)).getMessage(eq(WikiTagsTransformer.DATEFORMAT_DATETIME));
        verify(settings, times(1)).getActualTime();
    }

    @Test
    public void testPageName_PageTitle_unlinked() {
        PageName content = new PageName(Listable.PageNameFormat.PAGE_TITLE, false, false);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof TextOnly);
    }

    @Test
    public void testPageName_PageTitle_linked() {
        PageName content = new PageName(Listable.PageNameFormat.PAGE_TITLE, true, false);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof LinkPage);
    }

    @Test
    public void testPageName_PageFolder_unlinked() {
        PageName content = new PageName(Listable.PageNameFormat.PAGE_FOLDER, false, false);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof TextOnly);
    }

    @Test
    public void testPageName_PageFolder_linked() {
        PageName content = new PageName(Listable.PageNameFormat.PAGE_FOLDER, true, false);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof PageElementList);
    }

    @Test
    public void testPageName_PagePath_unlinked() {
        PageName content = new PageName(Listable.PageNameFormat.PAGE_PATH, false, false);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof TextOnly);
    }

    @Test
    public void testPageName_PagePath_linked() {
        PageName content = new PageName(Listable.PageNameFormat.PAGE_PATH, true, false);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof PageElementList);
    }

    @Test
    public void testPageTimestamp() throws Exception {
        PageTimestamp content = new PageTimestamp(false);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof TextOnly);
        verify(wikiService, times(1)).getWikiFile(eq("/path"));
        verify(messages, times(1)).getMessage(WikiTagsTransformer.DATEFORMAT_DATETIME);
    }

    @Test
    public void testListViewHistory_Empty_NullOutput() {
        when(wikiService.getLastViewedWikiFiles(anyInt())).thenReturn(Collections.emptyList());
        ListViewHistory content = new ListViewHistory(Listable.PageNameFormat.PAGE_TITLE, false, null, null, 3, null, null);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertNull(result.getChild());
        verify(wikiService, times(1)).getLastViewedWikiFiles(eq(3));
    }

    @Test
    public void testListViewHistory_Empty_Output() {
        when(wikiService.getLastViewedWikiFiles(anyInt())).thenReturn(Collections.emptyList());
        ListViewHistory content = new ListViewHistory(Listable.PageNameFormat.PAGE_TITLE, false, null, "output", 3, null, null);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof TextOnly);
    }

    @Test
    public void testListViewHistory_PageTitle_NotInline() {
        ArrayList<String> wikiPaths = new ArrayList<>();
        wikiPaths.add("/path1/name1");
        wikiPaths.add("/path2/name2");
        when(wikiService.getLastViewedWikiFiles(anyInt())).thenReturn(wikiPaths);
        ListViewHistory content = new ListViewHistory(Listable.PageNameFormat.PAGE_TITLE, false, null, null, 3, null, null);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof PageElementList);
        PageElementList pel = (PageElementList) result.getChild();
        assertEquals(pel.size(), 2);
        assertTrue(pel.get(0) instanceof ListItem);
        assertTrue(pel.get(1) instanceof ListItem);
        assertEquals(WikiHelper.getStringContent(result), "name1name2");
    }

    @Test
    public void testListViewHistory_PageFolder_Inline_NoSeparator() {
        ArrayList<String> wikiPaths = new ArrayList<>();
        wikiPaths.add("/path1/name1");
        wikiPaths.add("/path2/name2");
        when(wikiService.getLastViewedWikiFiles(anyInt())).thenReturn(wikiPaths);
        ListViewHistory content = new ListViewHistory(Listable.PageNameFormat.PAGE_FOLDER, true, null, null, 3, null, null);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof PageElementList);
        PageElementList pel = (PageElementList) result.getChild();
        assertEquals(pel.size(), 2);
        assertTrue(pel.get(0) instanceof LinkPage);
        assertTrue(pel.get(1) instanceof LinkPage);
        assertEquals(WikiHelper.getStringContent(result), "/path1//path2/");
    }

    @Test
    public void testListViewHistory_PagePath_Inline_NoSeparator() {
        ArrayList<String> wikiPaths = new ArrayList<>();
        wikiPaths.add("/path1/name1");
        wikiPaths.add("/path2/name2");
        when(wikiService.getLastViewedWikiFiles(anyInt())).thenReturn(wikiPaths);
        ListViewHistory content = new ListViewHistory(Listable.PageNameFormat.PAGE_PATH, true, "-", null, 3, null, null);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof PageElementList);
        PageElementList pel = (PageElementList) result.getChild();
        assertEquals(pel.size(), 3);
        assertTrue(pel.get(0) instanceof LinkPage);
        assertTrue(pel.get(1) instanceof TextOnly);
        assertTrue(pel.get(2) instanceof LinkPage);
        assertEquals(WikiHelper.getStringContent(result), "/path1/name1-/path2/name2");
    }

    @Test
    public void testListEditHistory_Empty_NullOutput() {
        when(wikiService.getLastModified(anyInt())).thenReturn(Collections.emptyList());
        ListEditHistory content = new ListEditHistory(Listable.PageNameFormat.PAGE_TITLE, false, null, null, 3, null, null);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertNull(result.getChild());
        verify(wikiService, times(1)).getLastModified(eq(3));
    }

    @Test
    public void testListEditHistory_Empty_Output() {
        when(wikiService.getLastModified(anyInt())).thenReturn(Collections.emptyList());
        ListEditHistory content = new ListEditHistory(Listable.PageNameFormat.PAGE_TITLE, false, null, "output", 3, null, null);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof TextOnly);
    }

    @Test
    public void testListEditHistory_PageTitle_NotInline() {
        ArrayList<String> wikiPaths = new ArrayList<>();
        wikiPaths.add("/path1/name1");
        wikiPaths.add("/path2/name2");
        when(wikiService.getLastModified(anyInt())).thenReturn(wikiPaths);
        ListEditHistory content = new ListEditHistory(Listable.PageNameFormat.PAGE_TITLE, false, null, null, 3, null, null);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof PageElementList);
        PageElementList pel = (PageElementList) result.getChild();
        assertEquals(pel.size(), 2);
        assertTrue(pel.get(0) instanceof ListItem);
        assertTrue(pel.get(1) instanceof ListItem);
        assertEquals(WikiHelper.getStringContent(result), "name1name2");
    }

    @Test
    public void testListEditHistory_PageFolder_Inline_NoSeparator() {
        ArrayList<String> wikiPaths = new ArrayList<>();
        wikiPaths.add("/path1/name1");
        wikiPaths.add("/path2/name2");
        when(wikiService.getLastModified(anyInt())).thenReturn(wikiPaths);
        ListEditHistory content = new ListEditHistory(Listable.PageNameFormat.PAGE_FOLDER, true, null, null, 3, null, null);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof PageElementList);
        PageElementList pel = (PageElementList) result.getChild();
        assertEquals(pel.size(), 2);
        assertTrue(pel.get(0) instanceof LinkPage);
        assertTrue(pel.get(1) instanceof LinkPage);
        assertEquals(WikiHelper.getStringContent(result), "/path1//path2/");
    }

    @Test
    public void testListEditHistory_PagePath_Inline_NoSeparator() {
        ArrayList<String> wikiPaths = new ArrayList<>();
        wikiPaths.add("/path1/name1");
        wikiPaths.add("/path2/name2");
        when(wikiService.getLastModified(anyInt())).thenReturn(wikiPaths);
        ListEditHistory content = new ListEditHistory(Listable.PageNameFormat.PAGE_PATH, true, "-", null, 3, null, null);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof PageElementList);
        PageElementList pel = (PageElementList) result.getChild();
        assertEquals(pel.size(), 3);
        assertTrue(pel.get(0) instanceof LinkPage);
        assertTrue(pel.get(1) instanceof TextOnly);
        assertTrue(pel.get(2) instanceof LinkPage);
        assertEquals(WikiHelper.getStringContent(result), "/path1/name1-/path2/name2");
    }

    @Test
    public void testListParents() throws Exception {
        WikiFile wikiFile = new WikiFile("/path", "content",
                new WikiPage("/path", null), new AnyFile("/path.txt"));
        wikiFile.getParents().add("/parent1");
        when(wikiService.getWikiFile(anyString())).thenReturn(wikiFile);
        ListParents content = new ListParents(null, Listable.PageNameFormat.PAGE_TITLE, false, null, null, false, null, null);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof PageElementList);
        assertEquals(WikiHelper.getStringContent(result), "parent1");
    }

    @Test
    public void testListChildren() throws Exception {
        WikiFile wikiFile = new WikiFile("/path", "content",
                new WikiPage("/path", null), new AnyFile("/path.txt"));
        wikiFile.getChildren().add("/child1");
        when(wikiService.getWikiFile(anyString())).thenReturn(wikiFile);
        ListChildren content = new ListChildren(null, Listable.PageNameFormat.PAGE_TITLE, false, null, null, false, null, null);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof PageElementList);
        assertEquals(WikiHelper.getStringContent(result), "child1");
    }

    @Test
    public void testListPages() {
        when(wikiService.getWikiFilePaths()).thenReturn(Collections.singleton("/path1"));
        ListPages content = new ListPages(null, Listable.PageNameFormat.PAGE_PATH, false, null, null, false, null, null);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof PageElementList);
        assertEquals(WikiHelper.getStringContent(result), "/path1");
        verify(wikiService, times(1)).getWikiFilePaths();
    }

    @Test
    public void testListWantedPages() throws Exception {
        when(wikiService.getWikiFilePaths()).thenReturn(Collections.singleton("/path1"));
        PageElementList wikiPageContent = new PageElementList();
        wikiPageContent.add(new LinkPage("/path1"));
        wikiPageContent.add(new LinkPage("/path2"));
        wikiPageContent.add(new LinkPage("/path3/"));
        WikiFile wikiFile = new WikiFile("/path1", "content",
                new WikiPage("/path1", wikiPageContent), new AnyFile("/path1.txt"));
        when(wikiService.getWikiFile(anyString())).thenReturn(wikiFile);
        ListWantedPages content = new ListWantedPages(Listable.PageNameFormat.PAGE_PATH, false, null, null, null, null);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof PageElementList);
        PageElementList pel = (PageElementList) result.getChild();
        assertEquals(pel.size(), 1);
        assertEquals(WikiHelper.getStringContent(result), "/path2");
    }

    @Test
    public void testListUnlinkedPages_NoHide() throws Exception {
        Set<String> filePaths = new HashSet<>();
        filePaths.add("/path1");
        filePaths.add("/path1-parent");
        filePaths.add("/path1-child");
        filePaths.add("/path2");
        when(wikiService.getWikiFilePaths()).thenReturn(filePaths);
        PageElementList wikiPageContent = new PageElementList();
        wikiPageContent.add(new LinkPage("/path2"));
        WikiFile wikiFile = new WikiFile("/path1", "content",
                new WikiPage("/path1", wikiPageContent), new AnyFile("/path1.txt"));
        wikiFile.getParents().add("/path1-parent");
        wikiFile.getParents().add("/path1-child");
        when(wikiService.getWikiFile(anyString())).thenReturn(wikiFile);
        ListUnlinkedPages content = new ListUnlinkedPages(false, false, Listable.PageNameFormat.PAGE_PATH, false, null, null, null, null);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof PageElementList);
        PageElementList pel = (PageElementList) result.getChild();
        assertEquals(pel.size(), 3);
        assertEquals(WikiHelper.getStringContent(result), "/path1/path1-child/path1-parent");
    }

    @Test
    public void testListUnlinkedPages_HideParentsChildren() throws Exception {
        Set<String> filePaths = new HashSet<>();
        filePaths.add("/path1");
        filePaths.add("/path1-parent");
        filePaths.add("/path1-child");
        filePaths.add("/path2");
        when(wikiService.getWikiFilePaths()).thenReturn(filePaths);
        PageElementList wikiPageContent = new PageElementList();
        wikiPageContent.add(new LinkPage("/path2"));
        WikiFile wikiFile = new WikiFile("/path1", "content",
                new WikiPage("/path1", wikiPageContent), new AnyFile("/path1.txt"));
        wikiFile.getParents().add("/path1-parent");
        wikiFile.getParents().add("/path1-child");
        when(wikiService.getWikiFile(anyString())).thenReturn(wikiFile);
        ListUnlinkedPages content = new ListUnlinkedPages(true, true, Listable.PageNameFormat.PAGE_PATH, false, null, null, null, null);
        WikiPage wikiPage = new WikiPage("/path", content);
        WikiPage result = wikiTagsTransformer.transformWikiPage(wikiPage);
        assertTrue(result.getChild() instanceof PageElementList);
        PageElementList pel = (PageElementList) result.getChild();
        assertEquals(pel.size(), 1);
        assertEquals(WikiHelper.getStringContent(result), "/path1");
    }
}
