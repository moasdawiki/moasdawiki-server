/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2025 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.handler;

import net.moasdawiki.base.Logger;
import net.moasdawiki.base.Messages;
import net.moasdawiki.base.Settings;
import net.moasdawiki.service.render.HtmlService;
import net.moasdawiki.service.search.PageDetails;
import net.moasdawiki.service.search.SearchService;
import net.moasdawiki.service.wiki.WikiHelper;
import net.moasdawiki.service.wiki.WikiService;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;

public class SearchHandlerTest {

    private SearchService searchService;
    private HtmlService htmlService;
    private SearchHandler searchHandler;

    @BeforeMethod
    public void beforeMethod() {
        Logger logger = new Logger(null);
        Settings settings = mock(Settings.class);
        Messages messages = mock(Messages.class);
        when(messages.getMessage(eq(SearchHandler.SUMMARY_ONE_KEY), any())).thenReturn(SearchHandler.SUMMARY_ONE_KEY);
        when(messages.getMessage(eq(SearchHandler.SUMMARY_MANY_KEY), any())).thenReturn(SearchHandler.SUMMARY_MANY_KEY);
        when(messages.getMessage(eq(SearchHandler.TITLE_KEY), any())).thenReturn(SearchHandler.TITLE_KEY);
        WikiService wikiService = mock(WikiService.class);
        searchService = mock(SearchService.class);
        htmlService = mock(HtmlService.class);
        searchHandler = new SearchHandler(logger, settings, messages, wikiService, searchService, htmlService);
    }

    @Test
    public void testNull() throws Exception {
        searchHandler.handleSearchRequest(null);
        verify(searchService, times(1)).searchInRepository(argThat(Set::isEmpty));
    }

    @Test
    public void testEmptyString() throws Exception {
        searchHandler.handleSearchRequest("");
        verify(searchService, times(1)).searchInRepository(argThat(Set::isEmpty));
    }

    @Test
    public void testGenerateSearchResultPageNoMatches() throws Exception {
        when(searchService.searchInRepository(any())).thenReturn(Collections.emptyList());
        searchHandler.handleSearchRequest("text");
        verify(htmlService).convertPage(argThat(wikiPage -> WikiHelper.getStringContent(wikiPage).contains(SearchHandler.SUMMARY_MANY_KEY)));
    }

    @Test
    public void testGenerateSearchResultPageSomeMatches() throws Exception {
        List<PageDetails> searchResult = new ArrayList<>();
        PageDetails.MatchingLine titleLine = new PageDetails.MatchingLine("some title words");
        List<PageDetails.MatchingLine> textLines = new ArrayList<>();
        textLines.add(new PageDetails.MatchingLine("matching line 1"));
        textLines.add(new PageDetails.MatchingLine("matching line 2"));
        searchResult.add(new PageDetails("/page", titleLine, textLines, 0));
        when(searchService.searchInRepository(any())).thenReturn(searchResult);
        searchHandler.handleSearchRequest("some");
        verify(htmlService).convertPage(argThat(wikiPage -> {
            String content = WikiHelper.getStringContent(wikiPage);
            return content.contains("some title words") && content.contains("matching line 1")
                    && content.contains("matching line 2");
        }));
    }
}
