/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2025 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.repository;

import net.moasdawiki.FileHelper;
import net.moasdawiki.base.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.nio.file.Path;

import static org.testng.Assert.*;

/**
 * Tests for RepositoryService with additional shadow repository.
 */
public class RepositoryServiceWithShadowTest {

    private static final String SHADOW_REPOSITORY_BASE_PATH = "src/test/resources/repository-shadow";

    private Path tempDir;
    private RepositoryService repositoryService;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        tempDir = FileHelper.createTempDirectoryAndCopyFiles("src/test/resources/repository-without-cache");
        repositoryService = new RepositoryService(new Logger(null), tempDir.toFile(), new File(SHADOW_REPOSITORY_BASE_PATH), true);
    }

    @AfterMethod
    public void afterMethod() {
        FileHelper.deleteDirectory(tempDir.toFile());
        tempDir = null;
    }

    @Test
    public void testConstructor() throws Exception {
        // Start service, it will create the cache file automatically
        assertEquals(repositoryService.getFiles().size(), 3);
        assertNotNull(repositoryService.getFile("/file-2020-01-01.txt"));
        assertNotNull(repositoryService.getFile("/file-only-in-shadow.txt"));
        assertEquals(repositoryService.readTextFile(new AnyFile("/file-2020-01-01.txt")), "content in repository-without-cache");
        assertEquals(repositoryService.readTextFile(new AnyFile("/file-only-in-shadow.txt")), "file-only-in-shadow");
        assertEquals(repositoryService.repository2FilesystemPath("/a", false), new File(tempDir.toString(), "a").getAbsolutePath());
        assertEquals(repositoryService.repository2FilesystemPath("/a", true), new File(SHADOW_REPOSITORY_BASE_PATH, "a").getAbsolutePath());
    }

    @Test
    public void testExistsFile() {
        {
            String filePath = "file-only-in-shadow.txt";
            assertTrue(repositoryService.existsFile(filePath));
        }
        {
            String filePath = "/file-2020-01-01.txt";
            assertTrue(repositoryService.existsFile(filePath));
        }
    }
}
