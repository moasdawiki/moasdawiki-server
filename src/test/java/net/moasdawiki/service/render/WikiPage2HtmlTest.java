/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2025 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.render;

import net.moasdawiki.base.Messages;
import net.moasdawiki.service.wiki.WikiService;
import net.moasdawiki.service.wiki.structure.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class WikiPage2HtmlTest {

    private Messages messages;
    private WikiService wikiService;

    @BeforeMethod
    public void beforeMethod() {
        messages = mock(Messages.class);
        when(messages.getMessage(any())).thenAnswer(invocation -> invocation.getArgument(0));
        wikiService = mock(WikiService.class);
    }

    @Test
    public void testGenerateHeading() {
        {
            PageElement contentPage = new Heading(1, null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<h1 id=\"-1\"></h1>");
        }
        {
            PageElement contentPage = new Heading(2, new TextOnly("heading"));
            String html = convertPageElement(contentPage);
            assertEquals(html, "<h2 id=\"heading\">heading</h2>");
        }
        {
            PageElement contentPage = new Heading(3, new TextOnly("heading"));
            String html = convertPageElement(contentPage);
            assertEquals(html, "<h3 id=\"heading\">heading</h3>");
        }
        {
            PageElement contentPage = new Heading(4, new TextOnly("heading"));
            String html = convertPageElement(contentPage);
            assertEquals(html, "<p id=\"heading\">heading</p>");
        }
        {
            PageElementList pel = new PageElementList();
            pel.add(new Heading(1, new TextOnly("heading")));
            pel.add(new Heading(1, new TextOnly("heading")));
            pel.add(new Heading(1, null));
            String html = convertPageElement(pel);
            assertEquals(html, "<h1 id=\"heading\">heading</h1>\n<h1 id=\"heading-1\">heading</h1>\n<h1 id=\"-1\"></h1>");
        }
    }

    @Test
    public void testGenerateSeparator() {
        PageElement contentPage = new Separator();
        String html = convertPageElement(contentPage);
        assertEquals(html, "<hr>");
    }

    @Test
    public void testGenerateVerticalSpace() {
        PageElement contentPage = new VerticalSpace();
        String html = convertPageElement(contentPage);
        assertEquals(html, "<div class=\"verticalspace\"></div>");
    }

    @Test
    public void testGenerateTask() {
        {
            PageElement contentPage = new Task(null, null, null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<div class=\"task open\"></div>");
        }
        {
            PageElement contentPage = new Task(Task.State.OPEN, null, "task description");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<div class=\"task open\">task description</div>");
        }
        {
            PageElement contentPage = new Task(Task.State.OPEN_IMPORTANT, "schedule", "task description");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<div class=\"task important\"><span class=\"schedule\">schedule</span>task description</div>");
        }
        {
            PageElement contentPage = new Task(Task.State.CLOSED, null, "task description");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<div class=\"task closed\">task description</div>");
        }
    }

    @Test
    public void testGenerateListItem() {
        {
            PageElement contentPage = new ListItem(1, false, null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<ul><li class=\"level1\"></li></ul>");
        }
        {
            PageElement contentPage = new ListItem(1, true, null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<ol start=\"1\"><li class=\"level1\"></li></ol>");
        }
        {
            PageElement contentPage = new ListItem(2, false, new TextOnly("content"));
            String html = convertPageElement(contentPage);
            assertEquals(html, "<ul><li class=\"level2\">content</li></ul>");
        }
        {
            PageElement contentPage = new ListItem(2, true, new TextOnly("content"));
            String html = convertPageElement(contentPage);
            assertEquals(html, "<ol start=\"1\"><li class=\"level2\">content</li></ol>");
        }
        {
            PageElementList pel = new PageElementList();
            pel.add(new ListItem(1, false, new TextOnly("line 1")));
            pel.add(new ListItem(1, false, new TextOnly("line 2")));
            pel.add(new ListItem(2, false, new TextOnly("line 3")));
            pel.add(new ListItem(1, false, new TextOnly("line 4")));
            String html = convertPageElement(pel);
            assertEquals(html, "<ul><li class=\"level1\">line 1</li></ul>\n" +
                    "<ul><li class=\"level1\">line 2</li></ul>\n" +
                    "<ul><li class=\"level2\">line 3</li></ul>\n" +
                    "<ul><li class=\"level1\">line 4</li></ul>");
        }
        {
            PageElementList pel = new PageElementList();
            pel.add(new ListItem(1, true, new TextOnly("line 1")));
            pel.add(new ListItem(1, true, new TextOnly("line 2")));
            pel.add(new ListItem(2, true, new TextOnly("line 3")));
            pel.add(new ListItem(3, true, new TextOnly("line 4")));
            pel.add(new ListItem(2, true, new TextOnly("line 5")));
            pel.add(new ListItem(1, true, new TextOnly("line 6")));
            String html = convertPageElement(pel);
            assertEquals(html, "<ol start=\"1\"><li class=\"level1\">line 1</li></ol>\n" +
                    "<ol start=\"2\"><li class=\"level1\">line 2</li></ol>\n" +
                    "<ol start=\"1\"><li class=\"level2\">line 3</li></ol>\n" +
                    "<ol start=\"1\"><li class=\"level3\">line 4</li></ol>\n" +
                    "<ol start=\"2\"><li class=\"level2\">line 5</li></ol>\n" +
                    "<ol start=\"3\"><li class=\"level1\">line 6</li></ol>");
        }
    }

    @Test
    public void testGetNextListItemSequence() {
        // invalid parameter values
        assertEquals(1, WikiPage2Html.getNextListItemSequence(0, null));
        assertEquals(1, WikiPage2Html.getNextListItemSequence(-1, null));
        assertEquals(1, WikiPage2Html.getNextListItemSequence(1, null));
        assertEquals(1, WikiPage2Html.getNextListItemSequence(0, new int[3]));
        assertEquals(1, WikiPage2Html.getNextListItemSequence(-1, new int[3]));
        assertEquals(1, WikiPage2Html.getNextListItemSequence(10, new int[3]));

        {
            // same level
            int[] listItemSequence = {0, 0, 0};
            assertEquals(1, WikiPage2Html.getNextListItemSequence(1, listItemSequence));
            assertEquals(2, WikiPage2Html.getNextListItemSequence(1, listItemSequence));
            assertEquals(3, WikiPage2Html.getNextListItemSequence(1, listItemSequence));
            assertEquals(4, WikiPage2Html.getNextListItemSequence(1, listItemSequence));
            assertEquals(1, WikiPage2Html.getNextListItemSequence(2, listItemSequence));
            assertEquals(2, WikiPage2Html.getNextListItemSequence(2, listItemSequence));
            assertEquals(3, WikiPage2Html.getNextListItemSequence(2, listItemSequence));
        }
        {
            // reset higher levels
            int[] listItemSequence = {0, 0, 0};
            assertEquals(1, WikiPage2Html.getNextListItemSequence(1, listItemSequence));
            assertEquals(1, WikiPage2Html.getNextListItemSequence(2, listItemSequence));
            assertEquals(2, WikiPage2Html.getNextListItemSequence(2, listItemSequence));
            assertEquals(2, WikiPage2Html.getNextListItemSequence(1, listItemSequence));
            assertEquals(1, WikiPage2Html.getNextListItemSequence(2, listItemSequence));
            assertEquals(2, WikiPage2Html.getNextListItemSequence(2, listItemSequence));
            assertEquals(3, WikiPage2Html.getNextListItemSequence(1, listItemSequence));
        }
    }

    @Test
    public void testGenerateTable() {
        {
            Table table = new Table();
            String html = convertPageElement(table);
            assertEquals(html, "<div class=\"table\"><table>\n" +
                    "  </table></div>");
        }
        {
            TableCell cell1 = new TableCell(new TextOnly("header cell 1"), true);
            TableCell cell2 = new TableCell(new TextOnly("header cell 2"), true, "cellparam");
            TableCell cell3 = new TableCell(new TextOnly("cell 3"));
            TableCell cell4 = new TableCell(new TextOnly("cell 4"));
            TableRow row1 = new TableRow();
            row1.addCell(cell1);
            row1.addCell(cell2);
            TableRow row2 = new TableRow("rowparam");
            row2.addCell(cell3);
            row2.addCell(cell4);
            Table table = new Table("tableparams");
            table.addRow(row1);
            table.addRow(row2);
            String html = convertPageElement(table);
            assertEquals(html, "<div class=\"table\"><table class=\"tableparams\">\n" +
                    "    <tr>\n" +
                    "      <th>header cell 1</th>\n" +
                    "      <th class=\"cellparam\">header cell 2</th>\n" +
                    "    </tr>\n" +
                    "    <tr class=\"rowparam\">\n" +
                    "      <td>cell 3</td>\n" +
                    "      <td>cell 4</td>\n" +
                    "    </tr>\n" +
                    "  </table></div>");
        }
        {
            TableCell cell11 = new TableCell(new TextOnly("h1"), true);
            TableCell cell12 = new TableCell(new TextOnly("h2"), true);
            TableCell cell13 = new TableCell(new TextOnly("h3"), true, "h3param");
            TableCell cell14 = new TableCell(new TextOnly("h4"), true);
            TableCell cell21 = new TableCell(new TextOnly("b1"));
            TableCell cell22 = new TableCell(new TextOnly("b2"));
            TableCell cell23 = new TableCell(new TextOnly("b3"), false, "b3param");
            TableCell cell24 = new TableCell(new TextOnly("b4"));
            TableRow row1 = new TableRow();
            row1.addCell(cell11);
            row1.addCell(cell12);
            row1.addCell(cell13);
            row1.addCell(cell14);
            TableRow row2 = new TableRow();
            row2.addCell(cell21);
            row2.addCell(cell22);
            row2.addCell(cell23);
            row2.addCell(cell24);
            Table table = new Table();
            table.addRow(row1);
            table.addRow(row2);
            table.addColumn(new TableColumn(TableColumn.Alignment.LEFT));
            table.addColumn(new TableColumn(TableColumn.Alignment.CENTER));
            table.addColumn(new TableColumn(TableColumn.Alignment.RIGHT));
            table.addColumn(new TableColumn());
            String html = convertPageElement(table);
            assertEquals(html, "<div class=\"table\"><table>\n" +
                    "    <tr>\n" +
                    "      <th class=\"left\">h1</th>\n" +
                    "      <th class=\"center\">h2</th>\n" +
                    "      <th class=\"h3param right\">h3</th>\n" +
                    "      <th>h4</th>\n" +
                    "    </tr>\n" +
                    "    <tr>\n" +
                    "      <td class=\"left\">b1</td>\n" +
                    "      <td class=\"center\">b2</td>\n" +
                    "      <td class=\"b3param right\">b3</td>\n" +
                    "      <td>b4</td>\n" +
                    "    </tr>\n" +
                    "  </table></div>");
        }
    }

    @Test
    public void testGenerateParagraph() {
        {
            Paragraph contentPage = new Paragraph(null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<div class=\"paragraph0\"></div>");
        }
        {
            Paragraph contentPage = new Paragraph(true, 0, false, new TextOnly("centered"), null, null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<div class=\"paragraph0 center\">centered</div>");
        }
        {
            PageElementList pel = new PageElementList();
            pel.add(new Paragraph(null));
            pel.add(new Paragraph(false, 1, true, null, null, null));
            String html = convertPageElement(pel);
            assertEquals(html, "<div class=\"paragraph0\"></div>\n" +
                    "<div class=\"verticalspace\"></div>\n" +
                    "<div class=\"paragraph1\"></div>");
        }
    }

    @Test
    public void testGenerateCode() {
        {
            PageElement contentPage = new Code(Code.ContentType.NONE, "final");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<div class=\"code\">final</div>");
        }
        {
            PageElement contentPage = new Code(Code.ContentType.NONE, "text&with<special\nchars\tetc");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<div class=\"code\">text&amp;with&lt;special<br>\n" +
                    "chars&nbsp;etc</div>");
        }
        {
            PageElement contentPage = new Code(Code.ContentType.HTML, "<tag>");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<div class=\"code\"><span class=\"code-xml-special-character\">&lt;</span><span class=\"code-xml-tag\">tag</span><span class=\"code-xml-special-character\">&gt;</span></div>");
        }
        {
            PageElement contentPage = new Code(Code.ContentType.INI, "a=b");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<div class=\"code\"><span class=\"code-ini-key\">a</span><span class=\"code-ini-delimiter\">=</span><span class=\"code-ini-value\">b</span></div>");
        }
        {
            PageElement contentPage = new Code(Code.ContentType.JAVA, "final");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<div class=\"code\"><span class=\"code-java-keyword\">final</span></div>");
        }
        {
            PageElement contentPage = new Code(Code.ContentType.JAVASCRIPT, "var");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<div class=\"code\"><span class=\"code-javascript-keyword\">var</span></div>");
        }
        {
            PageElement contentPage = new Code(Code.ContentType.PROPERTIES, "a=b");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<div class=\"code\"><span class=\"code-properties-key\">a</span><span class=\"code-properties-delimiter\">=</span><span class=\"code-properties-value\">b</span></div>");
        }
        {
            PageElement contentPage = new Code(Code.ContentType.XML, "<tag>");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<div class=\"code\"><span class=\"code-xml-special-character\">&lt;</span><span class=\"code-xml-tag\">tag</span><span class=\"code-xml-special-character\">&gt;</span></div>");
        }
        {
            PageElement contentPage = new Code(Code.ContentType.YAML, "name");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<div class=\"code\"><span class=\"code-yaml-key\">name</span></div>");
        }
    }

    @Test
    public void testGenerateBold() {
        {
            PageElement contentPage = new Bold(null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<b></b>");
        }
        {
            PageElement contentPage = new Bold(new TextOnly("content"));
            String html = convertPageElement(contentPage);
            assertEquals(html, "<b>content</b>");
        }
    }

    @Test
    public void testGenerateItalic() {
        {
            PageElement contentPage = new Italic(null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<i></i>");
        }
        {
            PageElement contentPage = new Italic(new TextOnly("content"));
            String html = convertPageElement(contentPage);
            assertEquals(html, "<i>content</i>");
        }
    }

    @Test
    public void testGenerateUnderlined() {
        {
            PageElement contentPage = new Underlined(null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<u></u>");
        }
        {
            PageElement contentPage = new Underlined(new TextOnly("content"));
            String html = convertPageElement(contentPage);
            assertEquals(html, "<u>content</u>");
        }
    }

    @Test
    public void testGenerateStrikethrough() {
        {
            PageElement contentPage = new Strikethrough(null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<s></s>");
        }
        {
            PageElement contentPage = new Strikethrough(new TextOnly("content"));
            String html = convertPageElement(contentPage);
            assertEquals(html, "<s>content</s>");
        }
    }

    @Test
    public void testGenerateMonospace() {
        {
            PageElement contentPage = new Monospace(null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<code></code>");
        }
        {
            PageElement contentPage = new Monospace(new TextOnly("content"));
            String html = convertPageElement(contentPage);
            assertEquals(html, "<code>content</code>");
        }
    }

    @Test
    public void testGenerateSmall() {
        {
            PageElement contentPage = new Small(null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<span class=\"small\"></span>");
        }
        {
            PageElement contentPage = new Small(new TextOnly("content"));
            String html = convertPageElement(contentPage);
            assertEquals(html, "<span class=\"small\">content</span>");
        }
    }

    @Test
    public void testGenerateColor() {
        {
            PageElement contentPage = new Color(Color.ColorName.RED, null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<span class=\"color-red\"></span>");
        }
        {
            PageElement contentPage = new Color(Color.ColorName.GREEN, new TextOnly("content"));
            String html = convertPageElement(contentPage);
            assertEquals(html, "<span class=\"color-green\">content</span>");
        }
    }

    @Test
    public void testGenerateStyle() {
        {
            PageElement contentPage = new Style(new String[]{}, null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<span class=\"\"></span>");
        }
        {
            PageElement contentPage = new Style(new String[]{"style1", "style2"}, new TextOnly("content"));
            String html = convertPageElement(contentPage);
            assertEquals(html, "<span class=\"style1 style2\">content</span>");
        }
    }

    @Test
    public void testGenerateNowiki() {
        {
            PageElement contentPage = new Nowiki(null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "");
        }
        {
            PageElement contentPage = new Nowiki("content");
            String html = convertPageElement(contentPage);
            assertEquals(html, "content");
        }
        {
            PageElement contentPage = new Nowiki("content<b>with@@tags");
            String html = convertPageElement(contentPage);
            assertEquals(html, "content&lt;b&gt;with@@tags");
        }
    }

    @Test
    public void testGenerateHtml() {
        PageElement contentPage = new Html("<b>bold</b>text", null, null);
        String html = convertPageElement(contentPage);
        assertEquals(html, "<b>bold</b>text");
    }

    @Test
    public void testGenerateHtmlTag() {
        PageElement contentPage = new HtmlTag("tagname", "attributes", new TextOnly("content"));
        String html = convertPageElement(contentPage);
        assertEquals(html, "<tagname attributes>content</tagname>");
    }

    @Test
    public void testGenerateLinkPage() {
        {
            PageElement contentPage = new LinkPage(null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a href=\"\"></a>");
        }
        {
            // link without context page -> empty url
            PageElement contentPage = new LinkPage("/pagePath");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a href=\"\">pagePath</a>");
        }
        {
            // link without context page -> empty url
            PageElement contentPage = new LinkPage("/pagePath", new TextOnly("alternative text"));
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a href=\"\">alternative text</a>");
        }
        {
            // absolute link with context page
            LinkPage linkPage = new LinkPage("/linkPath");
            PageElement contentPage = new WikiPage("/path/contextPage", linkPage);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a class=\"linknewpage\" href=\"/edit/linkPath\">linkPath</a>");
        }
        {
            // relative link with context page
            LinkPage linkPage = new LinkPage("linkPath", new TextOnly("alternative text"));
            PageElement contentPage = new WikiPage("/path/contextPage", linkPage);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a class=\"linknewpage\" href=\"/edit/path/linkPath\">alternative text</a>");
        }
        {
            PageElement contentPage = new LinkPage("/pagePath", "anchor", null, null, null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a href=\"#anchor\">pagePath#anchor</a>");
        }
        {
            LinkPage linkPage = new LinkPage("/path/");
            PageElement contentPage = new WikiPage("/path/contextPage", linkPage);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a href=\"/view/path/\">path</a>");
        }
    }

    @Test
    public void testGenerateLinkWiki() {
        {
            PageElement contentPage = new LinkWiki("startpage");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a href=\"/\">ViewPageHandler.wiki.startpage</a>");
        }
        {
            // editpage without context page -> no link
            PageElement contentPage = new LinkWiki("editpage");
            String html = convertPageElement(contentPage);
            assertEquals(html, "ViewPageHandler.wiki.editpage");
        }
        {
            // editpage with context page
            LinkWiki linkWiki = new LinkWiki("editpage");
            PageElement contentPage = new WikiPage("/path/contextPage", linkWiki);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a href=\"/edit/path/contextPage\">ViewPageHandler.wiki.editpage</a>");
        }
        {
            // editpage with context page
            LinkWiki linkWiki = new LinkWiki("editpage", new TextOnly("alternative text"));
            PageElement contentPage = new WikiPage("/path/contextPage", linkWiki);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a href=\"/edit/path/contextPage\">alternative text</a>");
        }
        {
            // newpage without context page
            PageElement contentPage = new LinkWiki("newpage");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a href=\"/edit/\">ViewPageHandler.wiki.newpage</a>");
        }
        {
            // newpage with context page
            LinkWiki linkWiki = new LinkWiki("newpage");
            PageElement contentPage = new WikiPage("/path/contextPage", linkWiki);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a href=\"/edit/path/\">ViewPageHandler.wiki.newpage</a>");
        }
        {
            PageElement contentPage = new LinkWiki("shutdown");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a href=\"/shutdown\">ViewPageHandler.wiki.shutdown</a>");
        }
        {
            PageElement contentPage = new LinkWiki("unknown");
            String html = convertPageElement(contentPage);
            assertEquals(html, "wiki:unknown?");
        }
    }

    @Test
    public void testGenerateLinkLocalFile() {
        {
            // without context page -> no link
            PageElement contentPage = new LinkLocalFile("/filePath");
            String html = convertPageElement(contentPage);
            assertEquals(html, "");
        }
        {
            // absolute link with context page
            LinkLocalFile linkLocalFile = new LinkLocalFile("/filePath", new TextOnly("alternative text"));
            PageElement contentPage = new WikiPage("/path/contextPage", linkLocalFile);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a class=\"linkfile\" href=\"/file/filePath\">alternative text</a>");
        }
        {
            // relative link with context page
            LinkLocalFile linkLocalFile = new LinkLocalFile("filePath");
            PageElement contentPage = new WikiPage("/path/contextPage", linkLocalFile);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a class=\"linkfile\" href=\"/file/path/filePath\">filePath</a>");
        }
    }

    @Test
    public void testGenerateLinkExternal() {
        {
            PageElement contentPage = new LinkExternal("https://moasdawiki.net/");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a class=\"linkexternal\" href=\"https://moasdawiki.net/\">https://moasdawiki.net/</a>");
        }
        {
            PageElement contentPage = new LinkExternal("https://moasdawiki.net/", new TextOnly("alternative text"));
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a class=\"linkexternal\" href=\"https://moasdawiki.net/\">alternative text</a>");
        }
        {
            PageElement contentPage = new LinkExternal("mailto:user@domain.org");
            String html = convertPageElement(contentPage);
            assertEquals(html, "<a class=\"linkemail\" href=\"mailto:user@domain.org\">user@domain.org</a>");
        }
    }

    @Test
    public void testGenerateXmlTag() {
        {
            PageElement contentPage = new XmlTag(null, "tag", null, null);
            String html = convertPageElement(contentPage);
            assertEquals(html, "");
        }
        {
            PageElement contentPage = new XmlTag("prefix", "tag", null, new TextOnly("content"));
            String html = convertPageElement(contentPage);
            assertEquals(html, "content");
        }
    }

    @Test
    public void testGenerateTextOnly() {
        PageElement contentPage = new TextOnly("content");
        String html = convertPageElement(contentPage);
        assertEquals(html, "content");
    }

    @Test
    public void testGenerateLineBreak() {
        PageElement contentPage = new LineBreak();
        String html = convertPageElement(contentPage);
        assertEquals(html, "<br>");
    }

    @Test
    public void testGenerateAnchor() {
        PageElement contentPage = new Anchor("anchorname");
        String html = convertPageElement(contentPage);
        assertEquals(html, "<span id=\"anchorname\"></span>");
    }

    @Test
    public void testGenerateImage() {
        {
            // without context page -> no url
            PageElement contentPage = new Image("/image.png");
            String html = convertPageElement(contentPage);
            assertEquals(html, "");
        }
        {
            // absolute url with context page
            Image image = new Image("/image.png");
            PageElement contentPage = new WikiPage("/path/contextPage", image);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<img src=\"/img/image.png\" alt=\"\">");
        }
        {
            // relative url with context page
            Image image = new Image("image.png");
            PageElement contentPage = new WikiPage("/path/contextPage", image);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<img src=\"/img/path/image.png\" alt=\"\">");
        }
        {
            Map<String, String> options = new HashMap<>();
            options.put("key1", "value1");
            options.put("key2", "value2");
            Image image = new Image("image.png", options);
            PageElement contentPage = new WikiPage("/path/contextPage", image);
            String html = convertPageElement(contentPage);
            assertEquals(html, "<img src=\"/img/path/image.png\" key1=\"value1\" key2=\"value2\" alt=\"\">");
        }
    }

    @Test
    public void testGenerateSearchInput() {
        PageElement contentPage = new SearchInput();
        String html = convertPageElement(contentPage);
        assertEquals(html, "<form method=\"get\" action=\"/search/\" enctype=\"application/x-www-form-urlencoded\" name=\"searchForm\"><input type=\"text\" name=\"text\" placeholder=\"ViewPageHandler.html.search\"></form>");
    }

    private String convertPageElement(PageElement pageElement) {
        HtmlWriter htmlWriter = new WikiPage2Html(messages, wikiService, false).generate(pageElement);
        htmlWriter.closeAllTags();
        StringBuilder sb = new StringBuilder();
        for (String line : htmlWriter.getBodyLines()) {
            if (sb.length() > 0) {
                sb.append('\n');
            }
            sb.append(line);
        }
        return sb.toString();
    }
}
