/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2025 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.render;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PascalFormatterTest {

    @Test
    public void testFormatKeywords() {
        for (String keyword : PascalFormatter.KEYWORDS) {
            String formatted = new PascalFormatter(keyword).format();
            assertEquals(formatted, "<span class=\"code-pascal-keyword\">" + keyword + "</span>");
        }
    }

    @Test
    public void testFormatNonKeywordText() {
        String formatted = new PascalFormatter("non-keyword-text").format();
        assertEquals(formatted, "non-keyword-text");
    }

    @Test
    public void testFormatLineBreak() {
        String formatted = new PascalFormatter("line1\nline2").format();
        assertEquals(formatted, "line1<br>\nline2");
    }

    @Test
    public void testFormatSingleLineComment() {
        {
            String formatted = new PascalFormatter("// single line comment").format();
            assertEquals(formatted, "<span class=\"code-pascal-comment\">//&nbsp;single&nbsp;line&nbsp;comment</span>");
        }
        {
            String formatted = new PascalFormatter("// with linebreak\n").format();
            assertEquals(formatted, "<span class=\"code-pascal-comment\">//&nbsp;with&nbsp;linebreak</span><br>\n");
        }
    }

    @Test
    public void testFormatMultipleLineComment() {
        {
            String codeText = "{ multi\n"
                    + "line\n"
                    + "comment }";
            String formatted = new PascalFormatter(codeText).format();
            assertEquals(formatted, "<span class=\"code-pascal-comment\">{&nbsp;multi</span><br>\n"
                    + "<span class=\"code-pascal-comment\">line</span><br>\n"
                    + "<span class=\"code-pascal-comment\">comment&nbsp;}</span>");
        }
        {
            String codeText = "{ one line\n"
                    + "break }";
            String formatted = new PascalFormatter(codeText).format();
            assertEquals(formatted, "<span class=\"code-pascal-comment\">{&nbsp;one&nbsp;line</span><br>\n"
                    + "<span class=\"code-pascal-comment\">break&nbsp;}</span>");
        }
        {
            String codeText = "{ one line }";
            String formatted = new PascalFormatter(codeText).format();
            assertEquals(formatted, "<span class=\"code-pascal-comment\">{&nbsp;one&nbsp;line&nbsp;}</span>");
        }
        {
            String codeText = "(* multi\n"
                    + "line\n"
                    + "comment *)";
            String formatted = new PascalFormatter(codeText).format();
            assertEquals(formatted, "<span class=\"code-pascal-comment\">(*&nbsp;multi</span><br>\n"
                    + "<span class=\"code-pascal-comment\">line</span><br>\n"
                    + "<span class=\"code-pascal-comment\">comment&nbsp;*)</span>");
        }
        {
            String codeText = "(* one line\n"
                    + "break *)";
            String formatted = new PascalFormatter(codeText).format();
            assertEquals(formatted, "<span class=\"code-pascal-comment\">(*&nbsp;one&nbsp;line</span><br>\n"
                    + "<span class=\"code-pascal-comment\">break&nbsp;*)</span>");
        }
        {
            String codeText = "(* one line *)";
            String formatted = new PascalFormatter(codeText).format();
            assertEquals(formatted, "<span class=\"code-pascal-comment\">(*&nbsp;one&nbsp;line&nbsp;*)</span>");
        }
    }

    @Test
    public void testFormatString() {
        {
            String formatted = new PascalFormatter("'text \\t content'").format();
            assertEquals(formatted, "<span class=\"code-pascal-string\">&apos;text&nbsp;\\t&nbsp;content&apos;</span>");
        }
        {
            String formatted = new PascalFormatter("'without end").format();
            assertEquals(formatted, "<span class=\"code-pascal-string\">&apos;without&nbsp;end</span>");
        }
        {
            String formatted = new PascalFormatter("'without end\n").format();
            assertEquals(formatted, "<span class=\"code-pascal-string\">&apos;without&nbsp;end</span><br>\n");
        }
    }
}
