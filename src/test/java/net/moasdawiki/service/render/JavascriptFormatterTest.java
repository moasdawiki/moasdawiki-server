/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2025 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.render;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class JavascriptFormatterTest {

    @Test
    public void testFormatKeywords() {
        for (String keyword : JavascriptFormatter.KEYWORDS) {
            String formatted = new JavascriptFormatter(keyword).format();
            assertEquals(formatted, "<span class=\"code-javascript-keyword\">" + keyword + "</span>");
        }
    }

    @Test
    public void testFormatNonKeywordText() {
        String formatted = new JavascriptFormatter("non-keyword-text").format();
        assertEquals(formatted, "non-keyword-text");
    }

    @Test
    public void testFormatLineBreak() {
        String formatted = new JavascriptFormatter("line1\nline2").format();
        assertEquals(formatted, "line1<br>\nline2");
    }

    @Test
    public void testFormatSingleLineComment() {
        {
            String formatted = new JavascriptFormatter("// single line comment").format();
            assertEquals(formatted, "<span class=\"code-javascript-comment\">//&nbsp;single&nbsp;line&nbsp;comment</span>");
        }
        {
            String formatted = new JavascriptFormatter("// with linebreak\n").format();
            assertEquals(formatted, "<span class=\"code-javascript-comment\">//&nbsp;with&nbsp;linebreak</span><br>\n");
        }
    }

    @Test
    public void testFormatMultipleLineComment() {
        {
            String codeText = "/* multi\n"
                    + "line\n"
                    + "comment */";
            String formatted = new JavascriptFormatter(codeText).format();
            assertEquals(formatted, "<span class=\"code-javascript-comment\">/*&nbsp;multi</span><br>\n"
                    + "<span class=\"code-javascript-comment\">line</span><br>\n"
                    + "<span class=\"code-javascript-comment\">comment&nbsp;*/</span>");
        }
        {
            String codeText = "/* one line\n"
                    + "*/";
            String formatted = new JavascriptFormatter(codeText).format();
            assertEquals(formatted, "<span class=\"code-javascript-comment\">/*&nbsp;one&nbsp;line</span><br>\n"
                    + "<span class=\"code-javascript-comment\">*/</span>");
        }
    }

    @Test
    public void testFormatString() {
        {
            String formatted = new JavascriptFormatter("\"text \\t content\"").format();
            assertEquals(formatted, "<span class=\"code-javascript-string\">&quot;text&nbsp;\\t&nbsp;content&quot;</span>");
        }
        {
            String formatted = new JavascriptFormatter("\"without end").format();
            assertEquals(formatted, "<span class=\"code-javascript-string\">&quot;without&nbsp;end</span>");
        }
        {
            String formatted = new JavascriptFormatter("\"without end\n").format();
            assertEquals(formatted, "<span class=\"code-javascript-string\">&quot;without&nbsp;end</span><br>\n");
        }
    }

    @Test
    public void testFormatChar() {
        String formatted = new JavascriptFormatter("'c'").format();
        assertEquals(formatted, "<span class=\"code-javascript-string\">&apos;c&apos;</span>");
    }
}
