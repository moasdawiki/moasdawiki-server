/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.wiki.parser;

import net.moasdawiki.service.wiki.WikiHelper;
import net.moasdawiki.service.wiki.structure.*;
import org.testng.annotations.Test;

import java.io.StringReader;
import java.util.List;

import static org.testng.Assert.*;

@SuppressWarnings("ConstantConditions")
public class WikiParserTableTest {

    @Test
    public void testParseTableBasics() throws Exception {
        {
            // empty table
            String text = "{|\n"
                    + "|}";
            PageElementList pel = new WikiParser(new StringReader(text)).parse();
            assertEquals(pel.size(), 1);
            assertTrue(pel.get(0) instanceof Table);
            Table table = (Table) pel.get(0);
            assertTrue(table.getRows().isEmpty());
        }
        {
            // table CSS params
            String text = "{|params\n"
                    + "|}";
            PageElementList pel = new WikiParser(new StringReader(text)).parse();
            assertEquals(pel.size(), 1);
            assertTrue(pel.get(0) instanceof Table);
            Table table = (Table) pel.get(0);
            assertEquals(table.getParams(), "params");
        }
    }

    @Test
    public void testParseTableCells() throws Exception {
        {
            // cells; row in one line
            String text = "{|\n"
                    + "|| h1 || h2 |-\n"
                    + "| a | b |-\n"
                    + "| c | d |-\n"
                    + "|}";
            PageElementList pel = new WikiParser(new StringReader(text)).parse();
            assertEquals(pel.size(), 1);
            assertTrue(pel.get(0) instanceof Table);
            Table table = (Table) pel.get(0);
            assertEquals(table.getRows().size(), 3);
            // header row
            assertEquals(table.getRows().get(0).getCells().size(), 2);
            assertTrue(table.getRows().get(0).getCells().get(0).isHeader());
            assertEquals(WikiHelper.getStringContent(table.getRows().get(0).getCells().get(0).getContent()).trim(), "h1");
            assertTrue(table.getRows().get(0).getCells().get(1).isHeader());
            assertEquals(WikiHelper.getStringContent(table.getRows().get(0).getCells().get(1).getContent()).trim(), "h2");
            // row 2
            assertEquals(table.getRows().get(1).getCells().size(), 2);
            assertFalse(table.getRows().get(1).getCells().get(0).isHeader());
            assertEquals(WikiHelper.getStringContent(table.getRows().get(1).getCells().get(0).getContent()).trim(), "a");
            assertFalse(table.getRows().get(1).getCells().get(1).isHeader());
            assertEquals(WikiHelper.getStringContent(table.getRows().get(1).getCells().get(1).getContent()).trim(), "b");
            // row 3
            assertEquals(table.getRows().get(2).getCells().size(), 2);
            assertFalse(table.getRows().get(2).getCells().get(0).isHeader());
            assertEquals(WikiHelper.getStringContent(table.getRows().get(2).getCells().get(0).getContent()).trim(), "c");
            assertFalse(table.getRows().get(2).getCells().get(1).isHeader());
            assertEquals(WikiHelper.getStringContent(table.getRows().get(2).getCells().get(1).getContent()).trim(), "d");
        }
        {
            // cells; row in multiple lines
            String text = "{|\n"
                    + "|| h1\n"
                    + "| b\n"
                    + "|-\n"
                    + "|}";
            PageElementList pel = new WikiParser(new StringReader(text)).parse();
            assertEquals(pel.size(), 1);
            assertTrue(pel.get(0) instanceof Table);
            Table table = (Table) pel.get(0);
            assertEquals(table.getRows().size(), 1);
            assertEquals(table.getRows().get(0).getCells().size(), 2);
            assertTrue(table.getRows().get(0).getCells().get(0).isHeader());
            assertEquals(WikiHelper.getStringContent(table.getRows().get(0).getCells().get(0).getContent()).trim(), "h1");
            assertFalse(table.getRows().get(0).getCells().get(1).isHeader());
            assertEquals(WikiHelper.getStringContent(table.getRows().get(0).getCells().get(1).getContent()).trim(), "b");
        }
        {
            // cell with multi line content
            String text = "{|\n"
                    + "| this text \n"
                    + "has more \n"
                    + "lines\n"
                    + "|-\n"
                    + "|}";
            PageElementList pel = new WikiParser(new StringReader(text)).parse();
            assertEquals(pel.size(), 1);
            assertTrue(pel.get(0) instanceof Table);
            Table table = (Table) pel.get(0);
            assertEquals(table.getRows().size(), 1);
            assertEquals(table.getRows().get(0).getCells().size(), 1);
            assertEquals(WikiHelper.getStringContent(table.getRows().get(0).getCells().get(0).getContent()).trim(), "this text has more lines");
        }
    }

    @Test
    public void testParseTableNested() throws Exception {
        String text = "{|\n"
                + "|{|inner-table\n"
                + "| inner-a | inner-b |-\n"
                + "|}\n"
                + "| outer-c |-\n"
                + "|}";
        PageElementList pel = new WikiParser(new StringReader(text)).parse();
        assertEquals(pel.size(), 1);
        assertTrue(pel.get(0) instanceof Table);
        Table table = (Table) pel.get(0);
        assertEquals(table.getRows().size(), 1);
        assertEquals(table.getRows().get(0).getCells().size(), 2);
        // inner table
        assertTrue(table.getRows().get(0).getCells().get(0).getContent() instanceof PageElementList);
        PageElementList innerPel = (PageElementList) table.getRows().get(0).getCells().get(0).getContent();
        assertEquals(innerPel.size(), 1);
        assertTrue(innerPel.get(0) instanceof Table);
        Table innerTable = (Table) innerPel.get(0);
        assertEquals(innerTable.getParams(), "inner-table");
        assertEquals(innerTable.getRows().size(), 1);
        assertEquals(innerTable.getRows().get(0).getCells().size(), 2);
        assertEquals(WikiHelper.getStringContent(innerTable.getRows().get(0).getCells().get(0).getContent()).trim(), "inner-a");
        assertEquals(WikiHelper.getStringContent(innerTable.getRows().get(0).getCells().get(1).getContent()).trim(), "inner-b");
        // outer cell
        assertEquals(WikiHelper.getStringContent(table.getRows().get(0).getCells().get(1).getContent()).trim(), "outer-c");
    }

    @Test
    public void testParseTableColumns() throws Exception {
        {
            String text = "{|\n"
                    + "||: h1 ||: h2 :|| h3 :|-\n"
                    + "|}";
            PageElementList pel = new WikiParser(new StringReader(text)).parse();
            assertEquals(pel.size(), 1);
            assertTrue(pel.get(0) instanceof Table);
            Table table = (Table) pel.get(0);
            List<TableColumn> columnList = table.getColumns();
            assertEquals(columnList.size(), 3);
            assertEquals(columnList.get(0).getAlignment(), TableColumn.Alignment.LEFT);
            assertEquals(columnList.get(1).getAlignment(), TableColumn.Alignment.CENTER);
            assertEquals(columnList.get(2).getAlignment(), TableColumn.Alignment.RIGHT);
            List<TableCell> cellList = table.getRows().get(0).getCells();
            assertEquals(cellList.size(), 3);
            assertEquals(WikiHelper.getStringContent(cellList.get(0).getContent()).trim(), "h1");
            assertEquals(WikiHelper.getStringContent(cellList.get(1).getContent()).trim(), "h2");
            assertEquals(WikiHelper.getStringContent(cellList.get(2).getContent()).trim(), "h3");
        }
        {
            String text = "{|\n"
                    + "|| : h1 || : h2 : || h3 : |-\n"
                    + "|}";
            PageElementList pel = new WikiParser(new StringReader(text)).parse();
            assertEquals(pel.size(), 1);
            assertTrue(pel.get(0) instanceof Table);
            Table table = (Table) pel.get(0);
            List<TableCell> cellList = table.getRows().get(0).getCells();
            assertEquals(cellList.size(), 3);
            assertEquals(WikiHelper.getStringContent(cellList.get(0).getContent()).trim(), ": h1");
            assertEquals(WikiHelper.getStringContent(cellList.get(1).getContent()).trim(), ": h2 :");
            assertEquals(WikiHelper.getStringContent(cellList.get(2).getContent()).trim(), "h3 :");
        }
    }
}
