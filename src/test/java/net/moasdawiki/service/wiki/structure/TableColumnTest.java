/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.wiki.structure;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class TableColumnTest {

    @Test
    public void testGetAlignment_DefaultConstructor() {
        TableColumn column = new TableColumn();
        assertNull(column.getAlignment());
    }

    @Test
    public void testGetAlignment_Param() {
        TableColumn column1 = new TableColumn(TableColumn.Alignment.LEFT);
        assertEquals(column1.getAlignment(), TableColumn.Alignment.LEFT);
        TableColumn column2 = new TableColumn(TableColumn.Alignment.CENTER);
        assertEquals(column2.getAlignment(), TableColumn.Alignment.CENTER);
        TableColumn column3 = new TableColumn(TableColumn.Alignment.RIGHT);
        assertEquals(column3.getAlignment(), TableColumn.Alignment.RIGHT);
    }

    @Test
    public void testCloneTyped() {
        TableColumn column = new TableColumn(TableColumn.Alignment.CENTER);
        TableColumn copy = column.cloneTyped();
        assertEquals(copy.getAlignment(), TableColumn.Alignment.CENTER);
        assertNotSame(copy, column);
    }
}
