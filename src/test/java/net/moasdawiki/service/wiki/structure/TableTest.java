/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.wiki.structure;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class TableTest {

    @Test
    public void testClonePageElement_Empty() {
        Table table = new Table();
        Table copy = table.cloneTyped();
        assertTrue(copy.getRows().isEmpty());
        assertTrue(copy.getColumns().isEmpty());
        assertNull(copy.getParams());
        assertNull(copy.getFromPos());
        assertNull(copy.getToPos());
    }

    @Test
    public void testClonePageElement_Params() {
        Table table = new Table("param1", 1, 2);
        Table copy = table.cloneTyped();
        assertEquals(copy.getParams(), "param1");
        assertEquals(copy.getFromPos(), 1);
        assertEquals(copy.getToPos(), 2);
    }

    @Test
    public void testClonePageElement_Rows() {
        Table table = new Table();
        table.newRow("param1");
        table.newRow("param2");
        Table copy = table.cloneTyped();
        assertEquals(copy.getRows().size(), 2);
        assertEquals(copy.getRows().get(0).getParams(), "param1");
        assertEquals(copy.getRows().get(1).getParams(), "param2");
        assertNotSame(copy.getRows().get(0), table.getRows().get(0));
        assertNotSame(copy.getRows().get(1), table.getRows().get(1));
    }

    @Test
    public void testClonePageElement_Columns() {
        Table table = new Table();
        table.addColumn(new TableColumn(TableColumn.Alignment.LEFT));
        table.addColumn(new TableColumn(TableColumn.Alignment.CENTER));
        table.addColumn(new TableColumn(TableColumn.Alignment.RIGHT));
        Table copy = table.cloneTyped();
        assertEquals(copy.getColumns().size(), 3);
        assertEquals(copy.getColumns().get(0).getAlignment(), TableColumn.Alignment.LEFT);
        assertEquals(copy.getColumns().get(1).getAlignment(), TableColumn.Alignment.CENTER);
        assertEquals(copy.getColumns().get(2).getAlignment(), TableColumn.Alignment.RIGHT);
        assertNotSame(copy.getColumns().get(0), table.getColumns().get(0));
        assertNotSame(copy.getColumns().get(1), table.getColumns().get(1));
        assertNotSame(copy.getColumns().get(2), table.getColumns().get(2));
    }
}
