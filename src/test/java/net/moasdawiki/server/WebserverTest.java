/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2025 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.server;

import net.moasdawiki.base.Logger;
import net.moasdawiki.base.Settings;
import net.moasdawiki.http.ContentType;
import net.moasdawiki.http.StatusCode;
import net.moasdawiki.service.HttpResponse;
import net.moasdawiki.service.render.HtmlService;
import net.moasdawiki.util.StringUtils;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.BasicHttpClientResponseHandler;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.jetbrains.annotations.NotNull;
import org.testng.annotations.*;

import java.util.concurrent.TimeoutException;
import java.util.function.BooleanSupplier;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

@SuppressWarnings("resource")
public class WebserverTest {
    private static final int PORT = 38081;

    private HtmlService htmlService;
    private RequestDispatcher requestDispatcher;
    private Webserver webserver;
    private Thread runner;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        Logger log = new Logger(null);
        Settings settings = mock(Settings.class);
        when(settings.getServerPort()).thenReturn(PORT);
        htmlService = mock(HtmlService.class);
        requestDispatcher = mock(RequestDispatcher.class);
        when(requestDispatcher.handleRequest(any())).thenReturn(new HttpResponse(StatusCode.OK));
        webserver = new Webserver(log, settings, htmlService, requestDispatcher);
        runner = new Thread(webserver::run);
        runner.start();
        waitFor(webserver::isRunning, 100);
        assertTrue(webserver.isRunning());
    }

    @AfterMethod
    public void afterMethod() throws Exception {
        webserver.stop();
        runner.join();
    }

    @Test
    public void testRun() throws Exception {
        assertTrue(webserver.isRunning());
        webserver.stop();
        runner.join();
        assertFalse(webserver.isRunning());
    }

    @Test
    public void testHandleConnectionRequest() throws Exception {
        HttpGet httpGet = new HttpGet("http://localhost:" + PORT + "/command");
        HttpClients.createDefault().execute(httpGet, response -> null);
        verify(requestDispatcher, times(1)).handleRequest(argThat(httpRequest -> httpRequest.getUrlPath().equals("/command")));
    }

    @Test
    public void testHandleConnectionResponseStatus() throws Exception {
        HttpResponse httpResponse = new HttpResponse(StatusCode.SERVER_NOT_IMPLEMENTED);
        when(requestDispatcher.handleRequest(any())).thenReturn(httpResponse);
        HttpGet httpGet = new HttpGet("http://localhost:" + PORT + "/command");
        int statusCode = HttpClients.createDefault().execute(httpGet, org.apache.hc.core5.http.HttpResponse::getCode);
        assertEquals(statusCode, 501);
    }

    @Test
    public void testHandleConnectionResponseHeadersNoCache() throws Exception {
        HttpResponse httpResponse = new HttpResponse(StatusCode.OK);
        when(requestDispatcher.handleRequest(any())).thenReturn(httpResponse);
        HttpGet httpGet = new HttpGet("http://localhost:" + PORT + "/command");
        ClassicHttpResponse receivedResponse = HttpClients.createDefault().execute(httpGet, response -> response);
        assertEquals(receivedResponse.getHeader("Cache-Control").getValue(), "no-cache");
        assertEquals(receivedResponse.getHeader("X-Content-Type-Options").getValue(), "nosniff");
        assertTrue(receivedResponse.containsHeader("Content-Security-Policy"));
        assertEquals(receivedResponse.getHeader("Cross-Origin-Opener-Policy").getValue(), "same-origin");
        assertEquals(receivedResponse.getHeader("Cross-Origin-Resource-Policy").getValue(), "same-origin");
    }

    @Test
    public void testHandleConnectionResponseHeadersCacheable() throws Exception {
        HttpResponse httpResponse = new HttpResponse(StatusCode.OK, ContentType.CSS, new byte[0]);
        when(requestDispatcher.handleRequest(any())).thenReturn(httpResponse);
        HttpGet httpGet = new HttpGet("http://localhost:" + PORT + "/command");
        ClassicHttpResponse receivedResponse = HttpClients.createDefault().execute(httpGet, response -> response);
        assertEquals(receivedResponse.getHeader("Cache-Control").getValue(), "max-age=86400");
    }

    @Test
    public void testHandleConnectionResponseContent() throws Exception {
        HttpResponse httpResponse = new HttpResponse(StatusCode.OK, ContentType.PLAIN, StringUtils.stringToUtf8Bytes("Hello"));
        when(requestDispatcher.handleRequest(any())).thenReturn(httpResponse);
        HttpGet httpGet = new HttpGet("http://localhost:" + PORT + "/command");
        String responseString = HttpClients.createDefault().execute(httpGet, new BasicHttpClientResponseHandler());
        assertEquals(responseString, "Hello");
    }

    @Test
    public void testShutdownCommand() throws Exception {
        assertTrue(webserver.isRunning());
        HttpResponse httpResponse = new HttpResponse(StatusCode.CLIENT_FORBIDDEN);
        when(htmlService.generateErrorPage(eq(StatusCode.CLIENT_FORBIDDEN), anyString())).thenReturn(httpResponse);
        HttpGet httpGet = new HttpGet("http://localhost:" + PORT + "/shutdown");
        int statusCode = HttpClients.createDefault().execute(httpGet, org.apache.hc.core5.http.HttpResponse::getCode);
        assertEquals(statusCode, 403);
    }

    @Test
    public void testShutdownRequestAllowed() throws Exception {
        webserver.setShutdownRequestAllowed(true);
        when(htmlService.generateMessagePage(anyString())).thenReturn(new HttpResponse(StatusCode.OK));
        HttpGet httpGet = new HttpGet("http://localhost:" + PORT + "/shutdown");
        HttpClients.createDefault().execute(httpGet, response -> null);
        waitFor(() -> !webserver.isRunning(), 100);
    }

    /**
     * Actively wait until the given condition becomes true or timeout.
     */
    @SuppressWarnings("SameParameterValue")
    private void waitFor(@NotNull BooleanSupplier condition, long timeoutInMs) throws Exception {
        long start = System.currentTimeMillis();
        while (!condition.getAsBoolean()){
            if (System.currentTimeMillis() - start > timeoutInMs){
                throw new TimeoutException("Timeout while waiting for condition");
            }
            synchronized(this) {
                //noinspection BusyWait
                Thread.sleep(1);
            }
        }
    }
}
