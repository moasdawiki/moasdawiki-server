/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2025 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.http;

import org.jetbrains.annotations.NotNull;

public abstract class StatusCodeUtils {
    /**
     * Derive the content type from the file name suffix.
     */
    @NotNull
    public static ContentType contentTypeByFileSuffix(@NotNull String filename) {
        int pos = filename.lastIndexOf('.');
        if (pos < 0) {
            return ContentType.BINARY;
        }
        String fileSuffix = filename.substring(pos).toLowerCase();
        switch (fileSuffix) {
            case ".css":
                return ContentType.CSS;
            case ".gif":
                return ContentType.IMAGE_GIF;
            case ".htm":
            case ".html":
                return ContentType.HTML;
            case ".ico":
                return ContentType.IMAGE_ICON;
            case ".jpg":
            case ".jpeg":
                return ContentType.IMAGE_JPEG;
            case ".js":
                return ContentType.JAVASCRIPT;
            case ".png":
                return ContentType.IMAGE_PNG;
            case ".pdf":
                return ContentType.PDF;
            case ".svg":
                return ContentType.IMAGE_SVG;
            case ".txt":
                return ContentType.PLAIN;
            case ".zip":
                return ContentType.ZIP;
            default:
                return ContentType.BINARY;
        }
    }
}
