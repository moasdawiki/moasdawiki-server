/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2025 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

/**
 * Helper methods to generate JavaScript Strings.
 */
public abstract class JavaScriptUtils {

	/**
	 * Generates a JSON array from a string list.
	 *
	 * @param list String list
	 * @return JavaScript array
	 */
	@NotNull
	public static String toArray(@NotNull List<String> list) {
		StringBuilder sb = new StringBuilder();
		sb.append('[');

		for (int i = 0; i < list.size(); i++) {
			if (i >= 1) {
				sb.append(", ");
			}
			sb.append('"');
			sb.append(escapeJavaScript(list.get(i)));
			sb.append('"');
		}

		sb.append(']');
		return sb.toString();
	}

	/**
	 * Escapes all JavaScript special characters.
	 *
	 * @param str String to be escaped
	 * @return Escaped string. null -> Parameter was null.
	 * @see "http://www.ietf.org/rfc/rfc4627.txt"
	 */
	@Nullable
	@Contract(value = "null -> null; !null -> !null", pure = true)
	public static String escapeJavaScript(@Nullable String str) {
		if (str == null) {
			return null;
		}

		StringBuilder sb = new StringBuilder(str.length());
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c == '\"') {
				sb.append("\\\"");
			} else if (c == '\'') {
				sb.append("\\'");
			} else if (c == '\\') {
				sb.append("\\\\");
			} else if (c == '\b') {
				sb.append("\\b");
			} else if (c == '\f') {
				sb.append("\\f");
			} else if (c == '\n') {
				sb.append("\\n");
			} else if (c == '\r') {
				sb.append("\\r");
			} else if (c == '\t') {
				sb.append("\\t");
			} else {
				sb.append(c);
			}
		}

		return sb.toString();
	}

	/**
	 * Generates a JSON string with a message.
	 *
	 * @param message message.
	 */
	public static String generateJsonMessage(@NotNull String message) {
		return generateJsonMap(Collections.singletonMap("message", message));
	}

	/**
	 * Generates a JSON string with a result code and a message.
	 *
	 * @param code    result code.
	 * @param message message.
	 */
	@NotNull
	public static String generateJsonCodeAndMessage(@NotNull Integer code, @Nullable String message) {
		Map<String, Object> keyValueMap = new HashMap<>();
		keyValueMap.put("code", code);
		if (message != null) {
			keyValueMap.put("message", message);
		}
		return generateJsonMap(keyValueMap);
	}

	/**
	 * Generates a JSON string from a key-value map.
	 */
	@NotNull
	public static String generateJsonMap(@NotNull Map<String, Object> keyValueMap) {
		List<String> keyList = new ArrayList<>(keyValueMap.keySet());
		keyList.sort(Comparator.naturalOrder());
		StringBuilder result = new StringBuilder();
		result.append("{ ");
		for (String key : keyList) {
			Object value = keyValueMap.get(key);
			if (value == null) {
				continue;
			}
			if (result.length() > 2) {
				result.append(", ");
			}
			result.append('"');
			result.append(escapeJsonValue(key));
			result.append("\": ");
			if (value instanceof String) {
				result.append('"');
				result.append(escapeJsonValue((String) value));
				result.append('"');
			}
			else if (value instanceof String[]) {
				String[] valueArray = (String[]) value;
				result.append('[');
				for (int i = 0; i < valueArray.length; i++) {
					if (i > 0) {
						result.append(", ");
					}
					result.append('"');
					result.append(escapeJsonValue(valueArray[i]));
					result.append('"');
				}
				result.append(']');
			} else {
				result.append(value);
			}
		}
		result.append(" }");
		return result.toString();
	}

	/**
	 * Escapes all special characters not allowed in a JSON value.
	 * Does NOT escape the single quote (') and the slash "/".
	 *
	 * @see "https://www.json.org/json-de.html"
	 */
	@NotNull
	static String escapeJsonValue(@NotNull String str) {
		StringBuilder sb = new StringBuilder(str.length());
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c == '\"') {
				sb.append("\\\"");
			} else if (c == '\\') {
				sb.append("\\\\");
			} else if (c == '\b') {
				sb.append("\\b");
			} else if (c == '\f') {
				sb.append("\\f");
			} else if (c == '\n') {
				sb.append("\\n");
			} else if (c == '\r') {
				sb.append("\\r");
			} else if (c == '\t') {
				sb.append("\\t");
			} else {
				sb.append(c);
			}
		}

		return sb.toString();
	}
}
