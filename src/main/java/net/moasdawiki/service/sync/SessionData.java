/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2025 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.sync;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

/**
 * Contains the data for a App session.
 */
public class SessionData {
	/**
	 * Server session ID.
	 */
	public String serverSessionId;

	/**
	 * App session ID.
	 */
	public String clientSessionId;

	/**
	 * Session created time.
	 */
	public Date createTimestamp;

	/**
	 * App name.
	 * null --> unknown.
	 */
	@Nullable
	public String clientName;

	/**
	 * App version.
	 * null --> unknown.
	 */
	@Nullable
	public String clientVersion;

	/**
	 * App phone host name.
	 * null --> unknown.
	 */
	public String clientHost;

	/**
	 * Has user granted access for the App?
	 */
	public boolean authorized;

	/**
	 * Last synchronization time.
	 * null --> never.
	 */
	public Date lastSyncTimestamp;
}
