/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2025 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.wiki.structure;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a table.
 */
public class Table extends PageElement {

	/**
	 * List of all table rows.
	 */
	@NotNull
	private final List<TableRow> rows;

	/**
	 * List of table columns.
	 * Table columns are only used for a unique formatting of all cells of a column.
	 * If the list is empty, no specific formatting rules apply.
	 */
	@NotNull
	private final List<TableColumn> columns;

	/**
	 * CSS classes for HTML formatting on table level.
	 * Several classes must be separated by a space.
	 * null -> no classes.
	 */
	@Nullable
	private final String params;

	public Table() {
		this(null, null, null);
	}

	public Table(@Nullable String params) {
		this(params, null, null);
	}

	public Table(@Nullable String params, @Nullable Integer fromPos, @Nullable Integer toPos) {
		super();
		this.rows = new ArrayList<>();
		this.columns = new ArrayList<>();
		this.params = params;
		this.fromPos = fromPos;
		this.toPos = toPos;
	}

	/**
	 * Add a row at the bottom of the table.
	 */
	public void addRow(@NotNull TableRow row) {
		rows.add(row);
		row.setParent(this);
	}

	/**
	 * Create a new row at the bottom of the table.
	 */
	public void newRow(@Nullable String rowParams) {
		addRow(new TableRow(rowParams));
	}

	@NotNull
	public List<TableRow> getRows() {
		return rows;
	}

	/**
	 * Add a column at the end.
	 */
	public void addColumn(@NotNull TableColumn column) {
		columns.add(column);
	}

	@NotNull
	public List<TableColumn> getColumns() {
		return columns;
	}

	/**
	 * Add a cell at the end of the last row.
	 */
	public void addCell(@NotNull TableCell cell) {
		if (rows.size() > 0) {
			rows.get(rows.size() - 1).addCell(cell);
		}
	}

	/**
	 * CSS classes for HTML formatting on table level.
	 * Several classes must be separated by a space.
	 *
	 * @return <code>null</code> -> no classes.
	 */
	@Nullable
	public String getParams() {
		return params;
	}

	public boolean isInline() {
		return false;
	}

	@NotNull
	public PageElement clonePageElement() {
		return cloneTyped();
	}

	public Table cloneTyped() {
		Table newTable = new Table(params, fromPos, toPos);
		for (TableRow row : rows) {
			newTable.addRow(row.cloneTyped());
		}
		for (TableColumn column : columns) {
			newTable.addColumn(column.cloneTyped());
		}
		return newTable;
	}
}
