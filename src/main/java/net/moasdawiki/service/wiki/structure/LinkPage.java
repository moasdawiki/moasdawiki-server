/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2025 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.wiki.structure;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Represents a link to another wiki page and/or an anchor position.
 */
public class LinkPage extends PageElementWithChild {

	/**
	 * Name of the wiki page to link to.
	 * If the name doesn't start with a '/', the link is relative to the wiki page that contains this link.
	 * null -> the link refers to the wiki page that contains this link, useful for anchor links.
	 */
	@Nullable
	private final String pagePath;

	/**
	 * Anchor name (without leading '#').
	 * null -> don't use anchor.
	 */
	@Nullable
	private final String anchor;

	public LinkPage(@Nullable String pagePath) {
		this(pagePath, null, null, null, null);
	}

	public LinkPage(@Nullable String pagePath, @Nullable PageElement alternativeText) {
		this(pagePath, null, alternativeText, null, null);
	}

	public LinkPage(@Nullable String pagePath, @Nullable String anchor, @Nullable PageElement alternativeText, @Nullable Integer fromPos, @Nullable Integer toPos) {
		super(alternativeText, fromPos, toPos);
		this.pagePath = pagePath;
		this.anchor = anchor;
	}

	/**
	 * Returns the wiki page name the links refers to.
	 */
	@Nullable
	public String getPagePath() {
		return pagePath;
	}

	/**
	 * Returns the anchor name.
	 */
	@Nullable
	public String getAnchor() {
		return anchor;
	}

	/**
	 * Returns the alternative text to be displayed instead of the wiki page name.
	 */
	@Nullable
	public PageElement getAlternativeText() {
		return getChild();
	}

	public boolean isInline() {
		return true;
	}

	@NotNull
	public PageElement clonePageElement() {
		if (child != null) {
			return new LinkPage(pagePath, anchor, child.clonePageElement(), fromPos, toPos);
		} else {
			return new LinkPage(pagePath, anchor, null, fromPos, toPos);
		}
	}
}
