/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2025 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.wiki.structure;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Describes the text color to draw the child element with.
 *
 * Example:
 * {{color:red}}...{{/color}}
 */
public class Color extends PageElementWithChild {

	/**
	 * Name of the color.
	 */
	@NotNull
	private final ColorName colorName;

	public Color(@NotNull ColorName colorName, @Nullable PageElement content) {
		this(colorName, content, null, null);
	}

	public Color(@NotNull ColorName colorName, @Nullable PageElement content, @Nullable Integer fromPos, @Nullable Integer toPos) {
		super(content, fromPos, toPos);
		this.colorName = colorName;
	}

	@NotNull
	public ColorName getColorName() {
		return colorName;
	}

	public boolean isInline() {
		return true;
	}

	@NotNull
	public PageElement clonePageElement() {
		if (child != null) {
			return new Color(colorName, child.clonePageElement(), fromPos, toPos);
		} else {
			return new Color(colorName, null, fromPos, toPos);
		}
	}

	/**
	 * Names of supported colors.
	 */
	@SuppressWarnings("unused")
	public enum ColorName {
		NONE, BLACK, BLUE, BROWN, CYAN, GREY, GREEN, GOLD, ORANGE, RED, WHITE, YELLOW
	}
}
