/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2025 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service;

import net.moasdawiki.http.ContentType;
import net.moasdawiki.http.StatusCode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Represents an HTTP response.
 * Contains the HTTP headers and the HTTP body if existing.
 */
public class HttpResponse {

	@NotNull
	private final StatusCode statusCode;

	@Nullable
	private String redirectUrl;

	@Nullable
	private ContentType contentType;

	private byte[] content; // HTTP body; null = empty

	/**
	 * Constructor.
	 */
	public HttpResponse(@NotNull StatusCode statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * Constructor with binary content.
	 */
	public HttpResponse(@NotNull StatusCode statusCode, @NotNull ContentType contentType, byte[] content) {
		this.statusCode = statusCode;
		this.contentType = contentType;
		this.content = content;
	}

	/**
	 * Constructor for redirect URL.
	 */
	public HttpResponse(@NotNull String redirectUrl) {
		this.statusCode = StatusCode.REDIRECTION_MOVED_TEMPORARILY;
		this.redirectUrl = redirectUrl;
	}

	@NotNull
	public StatusCode getStatusCode() {
		return statusCode;
	}

	@Nullable
	public String getRedirectUrl() {
		return redirectUrl;
	}

	@Nullable
	public ContentType getContentType() {
		return contentType;
	}

	public byte[] getContent() {
		return content;
	}
}
